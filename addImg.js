function addImg(event) {
    console.log("clicou");

    Swal.fire({
        title: 'Selecione um arquivo',
        showCancelButton: true,
        confirmButtonText: "Enviar",
        cancelButtonText:"Cancelar",
        confirmButtonColor : "#33691e",
        input: 'file',
        inputAttributes: {
            accept: "image/png"
        },
        willOpen: () => {
            $(".swal2-file").change(function () {
                var reader = new FileReader();
                reader.readAsDataURL(this.files[0]);
            });
        }

    }).then((file) => {
        if (file.value) {
            var formData = new FormData();
            var file = $(".swal2-file")[0].files[0];
            formData.append("arquivo", file);
            console.log(formData.get("arquivo"));
            $.ajax({
                type: "POST",
                url: "fileUpload.php",
                data: formData,
                contentType: false,
                cache: false,
                processData: false,
                success: function (response) {
                    Swal.fire("Enviado!", "Sua imagem foi enviada!", 'success');
                    jsonResponse = $.parseJSON(response);

                    //adiciona o card com a imagem no html
                    var card = $("#addImg").parent().before(`<div class='col s6 m4'><div class='card hoverable''><div class='card-image'><img src='imgs/${jsonResponse.fileName}'onclick='displayImg()'><a class='btn-floating halfway-fab waves-effect waves-light red' onclick='removeImg(event)'><i class='material-icons'>remove</i></a></div></div></div>`);

                    //repete o comando que faz os cards terem mesmas dimensoes
                    $('.card').css({'height':card.width()+'px'});

                },
                error: function() {
                    Swal.fire({ type: 'error', title: 'Oops...', text: 'Algo deu errado!' })
                }
            })
        }
    })

}

