<!DOCTYPE html>

<!--Guilherme Flach - 12/02/2021-->

<html lang="pt-br">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Envia Imagem</title>

  <!-- MATERIALIZE (FRAMEWORK CSS) -->
  <!-- Compiled and minified CSS -->
  <link rel="stylesheet" href=materialize/css/materialize.min.css">
  <!-- Compiled and minified JavaScript -->
  <script src="materialize/js/materialize.min.js"></script>
  <!-- FIM MATERIALIZE -->

  <!-- Material Icons-->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">


  <link rel="stylesheet" href="css/styles.css">
  <script src="js/jquery.min.js"></script>
  <script src = "removeImg.js"></script>
  <script src = "addImg.js"></script>
  <script src = "displayImg.js"></script>
</head>

<body>


  <div class="container z-depth-3">
    <div class="row">
    <?php
    //Passa por todas as imagens e colocas os cards delas
      $dir = new DirectoryIterator("imgs");
      foreach ($dir as $fileinfo) {
          if (!$fileinfo->isDot()) {
            echo "<div class='col s6 m4'><div class='card hoverable'><div class='card-image'><img src='imgs/".$fileinfo->getFilename()."' onclick='displayImg()'><a class='btn-floating halfway-fab waves-effect waves-light red' onclick='removeImg(event)'><i class='material-icons'>remove</i></a></div></div></div>";
          }
      }
    ?>

      <div class="col s6 m4">
        <div class="card hoverable red right-align waves-effect" id="addImg" onclick="addImg(event)">
          <div class="center-align">
            <a class="btn-floating red accent-3 z-depth-0"><i class="material-icons">add</i></a>
            <br>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Script do Materialize -->
  <script src="materialize/js/bin/materialize.min.js"></script>

  <!-- Script do SweetAlert-->
  <script src="node_modules\sweetalert2\dist\sweetalert2.all.min.js"></script>

  <!-- Scripts "Personalizados" -->
  <script src="js/main.js"></script>
</body>

</html>