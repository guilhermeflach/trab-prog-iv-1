<?php
  if(!is_uploaded_file($_FILES['arquivo']['tmp_name'])){
      header("location: index.html");
  }

  $extensao = pathinfo($_FILES['arquivo']['name'],PATHINFO_EXTENSION);

  if($extensao == "png"){
    $filename= uniqid();
    move_uploaded_file($_FILES['arquivo']['tmp_name'], "imgs/".$filename.".".$extensao);
    $responseArray = array(
      'fileName' => $filename.".".$extensao
    );

    echo json_encode($responseArray);
  }else{
      echo "O arquivo precisa ser png.";
  }

?>