function removeImg(event) {
    let imgSrc = $(event.target).parent().parent().find("img").attr("src");
    console.log(imgSrc);
    Swal.fire({
        title: 'Tem certeza que deseja excluir esta imagem?',
        showCancelButton: true,
        confirmButtonText: 'Excluir',    
        cancelButtonText:"Cancelar",
        focusConfirm:false,
        focusCancel: true,
        confirmButtonColor : "#d50000"
    }).then(function(result) {
        if(result.isConfirmed) {
            var formData = new FormData();
            formData.append("imgSrc", imgSrc);
            $.ajax({
                type:"POST",
                url: "fileRemove.php",
                data: formData,
                contentType: false,
                cache: false,
                processData: false,
                datatype:"html"
            }).then(
                Swal.fire("A imagem foi deletada!").then(location.reload()))
        }

    })
};